import {CapacitorConfig} from '@capacitor/cli';

const config: CapacitorConfig = {
    "appId": "io.jessicalabs.socartest",
    "appName": "socartest",
    webDir: 'dist',
    server: {
        androidScheme: 'https'
    }
};

export default config;
