import {Route} from 'react-router-dom';
import {IonApp, IonRouterOutlet, setupIonicReact} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';
import '@ionic/react/css/core.css';
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
import './theme/variables.css';
import {useEffect} from 'react';
import CarListPage from './features/car/CarListPage';
import './styles/App.css'

setupIonicReact();

import {App as CapacitorApp} from '@capacitor/app';
import {ConfigProvider} from "antd";
import 'antd/dist/antd.variable.min.css';
import CarDetailPage from "./features/car/CarDetailPage";

ConfigProvider.config({
    theme: {
        primaryColor: 'orange',
    }
});

export default function Entry() {

    useEffect(() => {
        CapacitorApp.addListener('backButton', ({canGoBack}) => {
            if (!canGoBack) {
                CapacitorApp.minimizeApp().then(res => console.log("minimizeApp===>", res));
            } else {
                window.history.back();
            }
        });
    }, [])

    return (
        <ConfigProvider>
            <IonApp>
                <IonReactRouter>
                    <IonRouterOutlet>
                        <Route exact path="/" component={CarListPage}/>
                        <Route exact path="/list" component={CarListPage}/>
                        <Route exact path="/CarDetailPage" component={CarDetailPage}/>
                    </IonRouterOutlet>
                </IonReactRouter>
            </IonApp>
        </ConfigProvider>
    )
}
