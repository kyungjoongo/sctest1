// @flow
import * as React from 'react';
import {TouchableOpacity, View} from "react-native-web";
import carService from "../CarService";
import {CloseOutlined} from "@ant-design/icons";
import {Button, Popover} from "antd";
import {Observer} from "mobx-react-lite";

type Props = {};

export function PriceButton(props: Props) {
    return (
        <Observer>
            {() => (

                <Popover
                    placement="bottomLeft"
                    content={
                        <View>
                            <View style={{flexDirection: 'row'}}>
                                <Button
                                    style={{
                                        backgroundColor: carService.priceSortType === 0 ? 'orange' : null,
                                        color: carService.priceSortType === 0 ? 'white' : null,
                                    }}
                                    onClick={() => {
                                        carService.priceSortType = 0;
                                        carService.___filterCarListByAllCondition()
                                    }}
                                >
                                    높은 가격순
                                </Button>
                                <View style={{width: 5,}}/>
                                <Button
                                    style={{
                                        backgroundColor: carService.priceSortType === 1 ? 'orange' : null,
                                        color: carService.priceSortType === 1 ? 'white' : null,
                                    }}
                                    onClick={() => {
                                        carService.priceSortType = 1;
                                        carService.___filterCarListByAllCondition()
                                    }}
                                >
                                    낮은 가격순
                                </Button>
                            </View>
                        </View>
                    }
                    title={<View style={{width: '100%', backgroundColor: 'transparent',}}>
                        <TouchableOpacity
                            onPress={() => {
                                carService.popOverButtons.showPriceTypes = false;
                            }}
                            style={{flexDirection: 'row'}}
                        >
                            <View style={{flex: .9}}>
                                <div>가격 소팅</div>
                            </View>
                            <View style={{flex: .1}}>
                                <CloseOutlined style={{fontSize: 25}}/>
                            </View>
                        </TouchableOpacity>
                    </View>}
                    trigger="click"
                    visible={carService.popOverButtons.showPriceTypes}
                    onVisibleChange={() => {
                        carService.popOverButtons.showPriceTypes = !carService.popOverButtons.showPriceTypes;
                    }}
                >
                    <View style={{margin: 10, backgroundColor: "transparent", padding: 0}}>
                        <Button
                            style={{
                                backgroundColor: carService.priceSortType >= 0 ? 'navy' : null,
                                color: carService.priceSortType >= 0 ? 'white' : null,
                            }}
                        >
                            가격
                        </Button>
                    </View>
                </Popover>
            )}
        </Observer>
    );
};
