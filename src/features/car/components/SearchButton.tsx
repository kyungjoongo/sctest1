// @flow
import * as React from 'react';
import {TouchableOpacity} from "react-native-web";
import carService from "../CarService";
import {HiMagnifyingGlass} from "react-icons/hi2";
import {Observer} from "mobx-react-lite";

type Props = {

};

export function SearchButton(props: Props) {
    return (
       <Observer>
           {()=>(
               <TouchableOpacity
                   onPress={() => {
                       carService.___filterCarListByAllCondition()
                   }}
                   style={{
                       height: 50,
                       backgroundColor: 'navy',
                       justifyContent: 'center',
                       alignItems: 'center',
                       flexDirection: 'row'
                   }}>
                   <HiMagnifyingGlass/>&nbsp;&nbsp;&nbsp;해당조건으로 검색
               </TouchableOpacity>
           )}
       </Observer>
    );
};
