// @flow
import * as React from 'react';
import {TouchableOpacity, View} from "react-native-web";
import carService from "../CarService";
import CheckableTag from "antd/es/tag/CheckableTag";
import {CloseOutlined} from "@ant-design/icons";
import {Button, Popover} from "antd";
import {Observer} from "mobx-react-lite";
import {isEmpty} from "lodash";

type Props = {

};

export function LocationButton(props: Props) {
    return (
        <Observer>
            {()=>(
                <Popover
                    placement="bottomLeft"
                    content={
                        <View>
                            <View style={{flexDirection: 'row'}}>
                                {carService.locationsTypes.map(tag => (
                                    <View style={{margin: 3,}} key={tag.toString()}>
                                        <CheckableTag
                                            key={tag}
                                            checked={carService.selectedLocationTypes.indexOf(tag) > -1}
                                            onChange={checked => carService.handleChangeLocationTag(tag, checked)}
                                        >
                                            {tag}
                                        </CheckableTag>
                                    </View>
                                ))}

                            </View>

                        </View>
                    }
                    title={<View style={{width: '100%', backgroundColor: 'transparent',}}>
                        <TouchableOpacity
                            onPress={() => {
                                carService.popOverButtons.showLocationTypes = false;
                            }}
                            style={{flexDirection: 'row'}}
                        >
                            <View style={{flex: .9}}>
                                <div>원하는 지역 선택하시오</div>
                            </View>
                            <View style={{flex: .1}}>
                                <CloseOutlined style={{fontSize: 25}}/>
                            </View>
                        </TouchableOpacity>
                    </View>}
                    trigger="click"
                    visible={carService.popOverButtons.showLocationTypes}
                    onVisibleChange={() => {
                        carService.popOverButtons.showLocationTypes = !carService.popOverButtons.showLocationTypes;
                    }}
                >
                    <View style={{margin: 10, backgroundColor: "transparent", padding: 0}}>
                        <Button

                            style={{
                                backgroundColor: !isEmpty(carService.selectedLocationTypes) ? 'navy' : null,
                                color: !isEmpty(carService.selectedLocationTypes) ? 'white' : null,
                            }}
                        >
                            대여지역
                        </Button>
                    </View>
                </Popover>

            )}
        </Observer>
    );
};
