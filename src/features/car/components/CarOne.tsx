// @flow
import * as React from 'react';
import {TouchableOpacity, View} from "react-native-web";
import {TypeCarOne} from "../Types";
import carService from "../CarService";
import {useHistory} from "react-router";

type Props = {
    item: TypeCarOne,
    index: number,
};

export function CarOne(props: Props) {

    const navigation = useHistory()

    return (
        <TouchableOpacity activeOpacity={0.9}
                          onPress={()=>{
                              carService.itemOne  =props.item
                              navigation.push('/CarDetailPage')
                          }}
                          style={{width: '100%', justifyContent: 'center'}} key={props.index.toString()}>
            <View style={{margin: 10, backgroundColor: 'orange'}}>
                <div style={{color: 'black', fontSize: 18, fontWeight: 'bold', textAlign: 'center'}}>
                    {props.item.carClassId}
                </div>
                <img src={props.item.image} alt={''} style={{width: 420}}/>
            </View>
            <View style={{marginLeft: 10,}}>
                <div>
                    {props.item.carClassId}
                </div>
            </View>
            <View style={{marginLeft: 10,}}>
                <div>
                    {props.item.regionGroups}
                </div>
            </View>
            <View style={{marginLeft: 10,}}>
                <div>
                    {props.item.year}
                </div>
            </View>
            <View style={{marginLeft: 10,}}>
                <div>
                    {props.item.carModel}
                </div>
            </View>
            <View style={{marginLeft: 10,}}>
                <div>
                    W {carService.numberWithCommas(props.item.price)}
                </div>
            </View>
        </TouchableOpacity>
    );
};
