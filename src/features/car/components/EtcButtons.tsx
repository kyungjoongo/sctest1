// @flow
import * as React from 'react';
import {View} from "react-native-web";
import {Button} from "antd";

type Props = {

};

export function EtcButtons(props: Props) {
    return (
        <>
            <View style={{margin: 10, backgroundColor: "transparent", padding: 0}}>
                <Button style={{}} onClick={() => {
                }}>
                    인기
                </Button>
            </View>
            <View style={{margin: 10, backgroundColor: "transparent", padding: 0}}>
                <Button style={{}} onClick={() => {
                }}>
                    특가
                </Button>
            </View>
            <View style={{margin: 10, backgroundColor: "transparent", padding: 0}}>
                <Button style={{}} onClick={() => {
                }}>
                    신차급
                </Button>
            </View>
            <View style={{margin: 10, backgroundColor: "transparent", padding: 0}}>
                <Button style={{}} onClick={() => {
                }}>
                    빠른대여
                </Button>
            </View>
        </>
    );
};
