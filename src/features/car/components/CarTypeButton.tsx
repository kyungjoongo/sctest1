// @flow
import * as React from 'react';
import {Button, Popover} from "antd";
import {TouchableOpacity, View} from "react-native-web";
import carService from "../CarService";
import CheckableTag from "antd/es/tag/CheckableTag";
import {CloseOutlined} from "@ant-design/icons";
import {Observer} from "mobx-react-lite";
import {isEmpty} from "lodash";

type Props = {};

export function CarTypeButton(props: Props) {
    return (
        <Observer>
            {() => (
                <Popover
                    placement="bottomLeft"
                    content={
                        <View>
                            <View style={{flexDirection: 'row'}}>
                                {carService.carTypes.map(tag => (
                                    <View style={{margin: 3,}} key={tag.toString()}>
                                        <CheckableTag
                                            key={tag}
                                            checked={carService.selectedCarTypes.indexOf(tag) > -1}
                                            onChange={checked => carService.handleChangeCarTag(tag, checked)}
                                        >
                                            {tag}
                                        </CheckableTag>
                                    </View>
                                ))}

                            </View>

                        </View>
                    }
                    title={<View style={{width: '100%', backgroundColor: 'transparent',}}>
                        <TouchableOpacity
                            onPress={() => {
                                carService.popOverButtons.showCarTypes = false;
                            }}
                            style={{flexDirection: 'row'}}
                        >
                            <View style={{flex: .9}}>
                                <div>원하는 차종을 선택하시오</div>
                            </View>
                            <View style={{flex: .1}}>
                                <CloseOutlined style={{fontSize: 25}}/>
                            </View>
                        </TouchableOpacity>
                    </View>}
                    trigger="click"
                    visible={carService.popOverButtons.showCarTypes}

                    onVisibleChange={() => {
                        carService.popOverButtons.showCarTypes = !carService.popOverButtons.showCarTypes;
                    }}
                >
                    <View style={{margin: 10, backgroundColor: "transparent", padding: 0}}>
                        <Button
                            style={{
                                backgroundColor: !isEmpty(carService.selectedCarTypes) ? 'navy' : null,
                                color: !isEmpty(carService.selectedCarTypes) ? 'white' : null,
                            }}

                            onClick={() => {
                                carService.isSelectedCarTypeFilter = !carService.isSelectedCarTypeFilter
                            }}
                        >
                            차종
                        </Button>
                    </View>
                </Popover>

            )}
        </Observer>
    );
};
