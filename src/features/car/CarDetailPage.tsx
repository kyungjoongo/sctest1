import {IonButton, IonButtons, IonContent, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import React, {useEffect} from "react";
import {Observer} from "mobx-react-lite";
import {View} from "react-native-web";
import carService from "./CarService";
import {ArrowLeftOutlined} from "@ant-design/icons";
import {useHistory} from "react-router";

export default function CarDetailPage() {

    const navigation = useHistory()

    useEffect(() => {
        init()
    }, []);

    async function init() {
    }


    return (
        <Observer>
            {() => {

                const {itemOne} = carService;

                return (
                    <IonPage>
                        <IonToolbar>
                            <IonButtons slot="start" onClick={()=>{
                                navigation.goBack();
                            }}>
                                <IonButton><ArrowLeftOutlined style={{fontSize:20}}/></IonButton>
                            </IonButtons>
                            <IonTitle>   {itemOne.carClassName}</IonTitle>

                        </IonToolbar>
                        <IonContent>
                            <View style={{marginVertical:30,}}>
                                <View style={{alignItems: 'center'}}>
                                    {itemOne.carModel}
                                </View>
                                <View style={{alignItems: 'center'}}>
                                    {itemOne.carClassName}
                                </View>
                                <View style={{alignItems: 'center'}}>
                                    {itemOne.price}
                                </View>
                                <View style={{alignItems: 'center'}}>
                                    {itemOne.regionGroups.toString()}
                                </View>
                                <View style={{alignItems: 'center'}}>
                                    {itemOne.drivingDistance}
                                </View>
                                <img src={itemOne?.image} alt={''}/>
                            </View>
                        </IonContent>
                    </IonPage>
                )
            }}
        </Observer>
    )
}
