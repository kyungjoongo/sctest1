import {IonContent, IonPage} from '@ionic/react';
import React, {useEffect} from "react";
import {useObserver} from "mobx-react-lite";
import {ActivityIndicator, ScrollView, View} from "react-native-web";
import carService from "./CarService";
import {TypeCarOne} from "./Types";
import {Button} from "antd";
import {isEmpty} from 'lodash'
import {CarOne} from "./components/CarOne";
import {LocationButton} from "./components/LocationButton";
import {CarTypeButton} from "./components/CarTypeButton";
import {PriceButton} from "./components/PriceButton";
import {EtcButtons} from "./components/EtcButtons";

export default function CarListPage() {

    useEffect(() => {
        init()
    }, []);

    async function init() {
        carService.getCarList().then(res => console.log("getCarList===>"));
    }

    const renderInitButton = () => (
        <View style={{margin: 10, backgroundColor: "transparent", padding: 0}}>
            <Button type={'dashed'} onClick={() => {
                carService.resetCarList()
            }}>
                초기화
            </Button>
        </View>
    )

    return useObserver(() => (
        <IonPage>
            <IonContent>
                <View style={{position: 'absolute', top: 0, width: 420}}>
                    <View style={{flexDirection: 'row'}}>
                        <ScrollView horizontal={true} style={{flex: 1}}>
                            <View style={{backgroundColor: '#e8dfdf', flexDirection: 'row',}}>
                                {renderInitButton()}
                                <CarTypeButton/>
                                <LocationButton/>
                                <PriceButton/>
                                <EtcButtons/>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View style={{marginTop: 70}}>
                    <ScrollView style={{height: window.innerHeight * 0.9,}}>
                        {carService.loading ?
                            <ActivityIndicator size={'large'} color={'orange'} style={{marginTop: 30}}/> :
                            carService?.filteredCarList.map((item: TypeCarOne, index: any) => {
                                return <CarOne item={item} index={index} key={index.toString()}/>
                            })
                        }
                        {!carService.loading && isEmpty(carService?.filteredCarList) &&
                            <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 100,}}>
                                <div style={{fontSize: 20,}}>
                                    조건에 맞는 차가 하나도 없네^^;
                                </div>
                            </View>
                        }
                    </ScrollView>
                </View>
            </IonContent>
        </IonPage>
    ))
}
