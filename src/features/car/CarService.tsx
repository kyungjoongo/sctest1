import {makeAutoObservable, toJS} from "mobx";
import axios from "axios";
import {TypeCarOne} from "./Types";
import {isEmpty} from "lodash";

class CarService {
    constructor() {
        makeAutoObservable(this)
    }

    loading: any = undefined;
    filteredCarList: TypeCarOne[] = [] //todo: 필터링 된 car list
    allCarList: TypeCarOne[] = []//todo: 전체  car list for filtering
    carTypes = ['경형/소형', '준준형', '중형/대형', '수입', 'SUV'];
    locationsTypes = ['서울/경기/인천', '부산/창원', '제주', '대구/경북', '대전', '광주'];
    priceTypes = ['낮은 가격순', '높은 가격순'];
    selectedCarTypes: any = [] //todo: 선택된 필터링 태그 리스트 (car)
    selectedLocationTypes: any = []//todo: 선택된 필터링 태그 리스트 (location)
    selectedPriceTypes: any = []//todo: 선택된 필터링 태그 리스트 (price)
    popOverButtons = {
        showCarTypes: false,
        showLocationTypes: false,
        showPriceTypes: false,
    }
    priceSortType = -1; //todo: 0: 높은 가격순, 1: 낮은가격순 , -1 :undefined
    isSelectedCarTypeFilter = false;


    /**
     * todo: 자동차 목록 fetch
     */
    async getCarList() {
        try {
            this.loading = true
            let res: any = await axios.get("https://api.jsonbin.io/v3/b/64ccbf55b89b1e2299cb2581")
            console.log("getCarList===>", res.data.record.carClasses)
            let cars: TypeCarOne[] = res.data.record.carClasses
            setTimeout(() => {
                this.filteredCarList = cars
                this.allCarList = cars
                this.loading = false;
            }, 150)
        } catch (e) {
            alert(e.toString() + "getCarList fetch error")
            this.loading = false;
        } finally {

        }
    }

    /**
     * todo: car type tag press event
     * @param tagOne
     * @param checked
     */
    handleChangeCarTag = (tagOne: string, checked: boolean) => {
        const _index = this.selectedCarTypes.indexOf(tagOne);
        if (_index === -1) {
            this.selectedCarTypes.push(tagOne)
        } else {
            const _index = this.selectedCarTypes.indexOf(tagOne);
            this.selectedCarTypes.splice(_index, 1);
        }
        this.___filterCarListByAllCondition()

    };


    /**
     * todo: location type tag press event
     * @param tagOne
     * @param checked
     */
    handleChangeLocationTag = (tagOne: string, checked: boolean) => {
        const _index = this.selectedLocationTypes.indexOf(tagOne);
        if (_index === -1) {
            this.selectedLocationTypes.push(tagOne)
        } else {
            const _index = this.selectedLocationTypes.indexOf(tagOne);
            this.selectedLocationTypes.splice(_index, 1);
        }

        this.___filterCarListByAllCondition()
    };

    resetCarList = () => {
        this.selectedCarTypes = []
        this.selectedLocationTypes = []
        this.selectedPriceTypes = []
        this.priceSortType = -1;
        this.getCarList().then(res => console.log("getCarList===>"))
    };

    ___filterCarListByAllCondition = () => {
        if (this.priceSortType >= 0 && isEmpty(this.selectedCarTypes) && isEmpty(this.selectedLocationTypes)) {
            this.filteredCarList = this.sortListByPrice(this.allCarList)
        } else if (!isEmpty(this.selectedCarTypes) && this.priceSortType >= 0 && isEmpty(this.selectedLocationTypes)) {
            let __filteredList: any = this.filterByCarType(this.allCarList)
            this.filteredCarList = this.sortListByPrice(__filteredList)
        } else if (!isEmpty(this.selectedLocationTypes) && this.priceSortType >= 0 && isEmpty(this.selectedCarTypes)) {
            let _filteredList2: any = this.filterByLocation(this.allCarList)
            this.filteredCarList = this.sortListByPrice(_filteredList2)
        } else if (!isEmpty(this.selectedCarTypes) && !isEmpty(this.selectedLocationTypes) && this.priceSortType >= 0) {
            let _filteredList1: any = this.filterByCarType(this.allCarList)
            let _filteredList2: any = this.filterByLocation(_filteredList1)
            this.filteredCarList = this.sortListByPrice(_filteredList2)
        } else if (!isEmpty(this.selectedCarTypes) && !isEmpty(this.selectedLocationTypes)) {
            let _filteredList1: any = this.filterByCarType(this.allCarList)
            this.filteredCarList = this.filterByLocation(_filteredList1)
        } else if (!isEmpty(this.selectedCarTypes)) {
            this.filteredCarList = this.filterByCarType(this.allCarList)
        } else if (!isEmpty(this.selectedLocationTypes)) {
            this.filteredCarList = this.filterByLocation(this.allCarList)
        }
    }

    sortListByPrice(_filteredList2: any) {
        let sortedList = []
        if (this.priceSortType === 0) {
            //todo : 높은 가격순
            sortedList = _filteredList2.sort(function (a: any, b: any) {
                return b.price - a.price;
            });
        } else if (this.priceSortType === 1) {
            //todo: 낮은 가격순
            sortedList = _filteredList2.sort(function (a: any, b: any) {
                return a.price - b.price;
            });
        }
        return sortedList
    }

    filterByCarType = (pCarList: any) => {
        let _filteredList1: any = []
        pCarList.map((itemOne: TypeCarOne, index: any) => {
            this.selectedCarTypes.map((typeOne: any, index: any) => {
                if (itemOne.carModel === typeOne) {
                    _filteredList1.push(itemOne)
                }
            })
        })
        return _filteredList1;
    }

    filterByLocation = (pFilterList: any) => {
        let locationFilterList: any = []
        pFilterList.map((itemOne: TypeCarOne, index: any) => {
            this.selectedLocationTypes.map((typeOne: any, index: any) => {
                if (itemOne.regionGroups.includes(typeOne)) {
                    locationFilterList.push(itemOne)
                }
            })
        })
        return locationFilterList;
    }

    numberWithCommas(x: any) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    itemOne: TypeCarOne = undefined

}

const carService = new CarService();

export default carService;
