export type TypeCarOne = {
    "carClassId": number,
    "carClassName": any,
    "carModel": string,
    "image": any,
    "drivingDistance": any,
    "year": any,
    "price": number,
    "discountPercent": any,
    "regionGroups": any,
    "carTypeTags": any,
}

